{
    "id": "2c2c4e07-136e-422f-9da3-a6dfeac43342",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f95b085a-8822-47f5-97bf-65aabc72e049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "74e59761-abe4-4c2d-8f43-35b7071069bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3589176a-cde5-4ff5-9c82-35a0f56fff36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 142,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0c28f90b-2f22-4094-876b-df50977743f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 131,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4e71aac0-0888-4376-9f1b-7481e7dcc148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "18672e7c-7280-4f55-b002-7789052ee1c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "bbf2000e-40b1-4a76-a48a-5fb0bf398373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 99,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b6306393-4d8c-4d5f-ad4d-03654a17f73f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 4,
                "shift": 9,
                "w": 3,
                "x": 94,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f6005713-68fa-4da3-b9e9-28b0d14bc353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 87,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c5e93a2e-bcac-4bba-86f4-c017fcc60e23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 80,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6a46ae34-4ea2-4c10-894e-68b452e6eb4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 154,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7c0f138c-71cc-44cd-9a22-bd129adb443f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 69,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7c43dd1a-7257-4056-990c-9682e8b6993a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 53,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7366f619-d0ca-457b-94f6-aa539ec8fe00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 46,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4d7191b7-ba3a-4418-83e0-4c670e897ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 41,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "58a73169-0611-4ba1-8c8d-8f2e994560db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 31,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6c129891-3d21-4b2b-a1c1-fd097d3acf77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c944836f-28e2-40ef-8fad-4b705c78e22d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "db194bbe-874e-4245-95da-e00b40aeb674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "49e289a2-a51c-46bd-9f2f-8114e39797fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 243,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9fa01f96-3fb2-4b5f-a689-518c4eac495d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 232,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "472fb27f-2732-47e1-8681-632773b3a090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "20bca9fe-8662-49cf-bd00-e3f6d732a072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 163,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d0c40514-4bf4-4820-a0e9-731ecc848a7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 173,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6a66a657-a984-4c83-9ed8-03315031c682",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 183,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "db305786-b431-4371-a697-8b5424567d15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 129,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "42ad53c9-aafa-4917-995f-805bfa989348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 124,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d5d5d909-21d4-46f1-abcb-d0d5a7590dc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 117,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6a8db7df-1c8c-49a9-92f3-10353a155ff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 108,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "88961173-bd5e-47f1-a10b-5cbe63defef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 99,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "34be8cb0-3a03-47a4-b20b-dc91cfc6aeb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 90,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d67c9af8-30b8-4199-a57d-aa572ff24db0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 82,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "924dab15-776e-4481-b554-bfaefcd85258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 71,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c97c8d2a-c5dc-48ed-8b22-b113d8e518b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 60,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e6f79030-6a69-4aae-b89f-72fe0ba0eeab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 51,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "638176e8-c879-484d-88b3-461fd9f5e004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 41,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "23a609c4-41bf-4c14-8b0c-5ba12f12ea58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 30,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ba77f0ce-ee87-4ed5-82d3-fb79e28e8596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 21,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "be6f577f-0041-43bf-a96a-42484fad7edc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 12,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "745ce260-f24c-467b-92da-04744387b79b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a913d0eb-9f5b-4595-949e-5506edd898c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 240,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f6c05d0b-23c4-465c-a376-a2a70852c9fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 231,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "024a0ff4-86f5-4366-ae34-f951bbd58fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 223,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a4d03627-f3ed-4070-8c7f-5f5234febd93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 213,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d74edd70-0865-46e2-bf87-94ab2f801414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 204,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f6031ee4-a0d1-4e81-afe9-a7fa5ebb4c85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "583c59e1-6e24-4c0a-ad0e-b90211142384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 222,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "35b3b78d-9c1c-4b70-b26d-1a6aabba2204",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "682736e3-f4b3-4a35-ae5e-fde1f9a4db0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 202,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "49c5005e-b205-4397-9606-cd88673a5a24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0c7bb8e9-a660-452a-84ae-7eba510e83bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9bd8ca42-aa05-4713-8081-0d5f03fecfbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "62f227e6-51c1-4488-9b2b-a9d3036d981b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4e77719c-2b5a-4cc8-9e74-fb3780327648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bd6da582-4af2-483b-9577-13f11ab104f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e78568f6-906e-40fa-b0a6-144be8106e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0d1a2d54-df86-4183-85da-91615838462e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "da8f389a-bc1f-45c8-b895-73653c2f1272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "fa3f09ff-9a83-48c6-a811-221f72c0cd17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "54c53caa-cf0e-4d1a-9387-c02a398c1930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d8b41a3c-936f-4198-8ebc-de3c58e2be0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bb5b0b16-a3c0-49c2-bfe4-859f06b703d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "798c2c2e-298b-42f5-8d6f-150f3f658396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "87bdcc0e-4f9d-4e34-8109-2c8da001ecd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "96dcbe1b-2895-432e-94d4-15310d95dc3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "32a9d6ed-4c1e-493a-85b8-947b79cc4283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5be79e8c-31d6-4486-9b89-7662c3e2ee26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "13fb5231-a51b-44cd-9bdc-c2fe876a43b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "22a49764-ab06-4384-b4e5-9a0b9cf3a51a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f2bebd0b-2d8b-41aa-a79b-b5ee2d47cac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "97416205-4b4f-4fd3-b4dc-b747ec5b6a98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "24c0ec42-12e7-4f72-860e-f62dc9802564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "fab09a4c-7f66-4224-8bb4-98460548b2d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 87,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "17233a62-019c-4404-8821-8476d38889d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4114afd7-eeaf-4d67-b5f0-6ded9a81158f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 185,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c77bb731-0eae-43dc-a624-e0d83c27069e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 175,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "46d5ffb9-6176-4917-8bd1-d914e6c76789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 166,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d71eb111-0749-499d-87d1-39eced52909d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 155,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2dd7d5d2-d218-4049-97b6-ee101f07a977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 146,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "563e193f-a9e2-4762-8a62-c9bcd7e8e5ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 135,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6f1c6f02-37a8-4a55-a3b6-a1f00bd28fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 126,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "39d422c5-ae6f-4033-9e22-6d1aa2373ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 116,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c6ab5e7b-a2e2-48c5-9c04-882558ddef43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 8,
                "x": 106,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e33295ac-0329-4b80-a210-554666190bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 193,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2aaa0550-47bf-4f40-9a8a-0cdee4e3cb7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 96,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5086ce32-45b4-41a4-a025-10d4c359f9ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 78,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a2deff1c-be0b-45a9-8d9c-8f846b1dd7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 67,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "890b6668-c900-4537-a904-8b28ab6385b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 56,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "59f4a2bb-0bb3-4e9f-ab12-4904108d9cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 45,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4bb74d78-aa8b-422a-bc4b-7f8c8549ac63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 34,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d6cd2f72-91b0-4281-9edf-634fb8221439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 25,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "47176316-3265-403c-a9f5-8b39b918d6c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 16,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8036dea6-c16b-474b-92a6-e1b058b40beb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 4,
                "shift": 9,
                "w": 3,
                "x": 11,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d5aadd26-59ec-45eb-88bd-4ce9fdee5cce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e250b560-4e10-46cc-a79c-103f7b6498c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 139,
                "y": 65
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "e3ce07b2-84a8-4f44-a463-bf12d282b0cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 150,
                "y": 65
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯ YO YO",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}