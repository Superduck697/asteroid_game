{
    "id": "c03d6387-9a39-435f-856d-ee06daab48e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 2,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b96b1e88-bd33-4310-a11a-fd0af5d96692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c03d6387-9a39-435f-856d-ee06daab48e7",
            "compositeImage": {
                "id": "361f0453-f2d6-47e0-852b-c02eaf3263bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96b1e88-bd33-4310-a11a-fd0af5d96692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cba23685-f1b4-4e8c-aab4-800241e1ad94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96b1e88-bd33-4310-a11a-fd0af5d96692",
                    "LayerId": "5b408383-467f-450d-ad01-05807c6f31f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5b408383-467f-450d-ad01-05807c6f31f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c03d6387-9a39-435f-856d-ee06daab48e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}