{
    "id": "33fe6315-4fc0-481a-8e91-dd08b8a3eb70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c05e0a99-4b24-4bfc-9483-a2497c8f3980",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33fe6315-4fc0-481a-8e91-dd08b8a3eb70",
            "compositeImage": {
                "id": "f9f00e10-ec2c-45a7-9d7d-44d934f81dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c05e0a99-4b24-4bfc-9483-a2497c8f3980",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b90c621-7b82-445e-a529-e2aba3d4ff2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c05e0a99-4b24-4bfc-9483-a2497c8f3980",
                    "LayerId": "264ebead-0a40-4cd7-8fd5-55819879b048"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "264ebead-0a40-4cd7-8fd5-55819879b048",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33fe6315-4fc0-481a-8e91-dd08b8a3eb70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}