{
    "id": "a54a8861-b3a2-4e3f-afb4-8f4054c53a90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3aadec22-d16a-4410-8a0f-1640d7da2d84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a54a8861-b3a2-4e3f-afb4-8f4054c53a90",
            "compositeImage": {
                "id": "1a330a4a-bd03-4993-bde0-639544c8157f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aadec22-d16a-4410-8a0f-1640d7da2d84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "159acb31-3ad5-4ff0-9e71-32fe92c9ff57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aadec22-d16a-4410-8a0f-1640d7da2d84",
                    "LayerId": "c6ec0546-89cd-4fda-bdb2-1dccaaee9b1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "c6ec0546-89cd-4fda-bdb2-1dccaaee9b1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a54a8861-b3a2-4e3f-afb4-8f4054c53a90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}