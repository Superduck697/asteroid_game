{
    "id": "96a85fe5-c9a9-46ce-a757-de4456b39985",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6035407a-6ab8-4f9e-a340-5d25afd13c67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a85fe5-c9a9-46ce-a757-de4456b39985",
            "compositeImage": {
                "id": "5a8d8121-b6a9-45c1-a71c-ab8fa1dbee24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6035407a-6ab8-4f9e-a340-5d25afd13c67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e3ba9b4-da76-45fa-b4ca-a962423afdf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6035407a-6ab8-4f9e-a340-5d25afd13c67",
                    "LayerId": "25440af7-3f04-4366-99d6-9fef9fb994b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "25440af7-3f04-4366-99d6-9fef9fb994b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96a85fe5-c9a9-46ce-a757-de4456b39985",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}