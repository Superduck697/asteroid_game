{
    "id": "4f99b6c4-6866-49f0-b22c-6df569fb1753",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48f22d85-13f5-45b3-bb4c-8e10e0e036df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f99b6c4-6866-49f0-b22c-6df569fb1753",
            "compositeImage": {
                "id": "adc484cb-527d-4ab1-8373-cd47fe2be20f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48f22d85-13f5-45b3-bb4c-8e10e0e036df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a32ae16-ad82-46f9-81a2-799681486516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48f22d85-13f5-45b3-bb4c-8e10e0e036df",
                    "LayerId": "13cebccd-a4d0-40ca-9b11-b1c9a382cf07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "13cebccd-a4d0-40ca-9b11-b1c9a382cf07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f99b6c4-6866-49f0-b22c-6df569fb1753",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}