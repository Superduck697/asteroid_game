{
    "id": "51c1c4d0-bdd6-46b1-90d5-bd51b575d68a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 32,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f248f19c-0ade-4ba9-9891-cafd516f36bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51c1c4d0-bdd6-46b1-90d5-bd51b575d68a",
            "compositeImage": {
                "id": "d35fc5d5-9173-4fa5-8f89-32de76659c52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f248f19c-0ade-4ba9-9891-cafd516f36bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e47b4e8-24b5-46ed-b77e-63a68d62915e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f248f19c-0ade-4ba9-9891-cafd516f36bf",
                    "LayerId": "8ef6466f-0179-4a60-90b5-0c3c7a3ca04b"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "8ef6466f-0179-4a60-90b5-0c3c7a3ca04b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51c1c4d0-bdd6-46b1-90d5-bd51b575d68a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}