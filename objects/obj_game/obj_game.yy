{
    "id": "e0907e1e-8e73-45bb-a8a2-844a65477967",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game",
    "eventList": [
        {
            "id": "2473fbe7-609e-48dd-930c-2a2aa2bf2568",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0907e1e-8e73-45bb-a8a2-844a65477967"
        },
        {
            "id": "7dc019ac-e0f3-4331-ab96-6a7e4efa9cbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e0907e1e-8e73-45bb-a8a2-844a65477967"
        },
        {
            "id": "d5a52a93-b3c1-4cdf-b59b-bc5e06f9db52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0907e1e-8e73-45bb-a8a2-844a65477967"
        },
        {
            "id": "39e65c89-e181-4d84-8842-f1b56b6f9712",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "e0907e1e-8e73-45bb-a8a2-844a65477967"
        },
        {
            "id": "88de008f-38f1-4757-8974-579327b1649e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e0907e1e-8e73-45bb-a8a2-844a65477967"
        },
        {
            "id": "4645172d-703f-471f-abb2-5c0533de1c94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "e0907e1e-8e73-45bb-a8a2-844a65477967"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}