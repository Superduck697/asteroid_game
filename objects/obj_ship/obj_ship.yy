{
    "id": "64473d14-f9af-4f87-834f-d70d32c16719",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "381e2755-5c75-49e9-bd74-4826eebd5c6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "64473d14-f9af-4f87-834f-d70d32c16719"
        },
        {
            "id": "bb3ca60e-1b45-4891-a51f-a8fbb1beb685",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4ecd40f5-f347-4256-a465-1f1d63b40496",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "64473d14-f9af-4f87-834f-d70d32c16719"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "51c1c4d0-bdd6-46b1-90d5-bd51b575d68a",
    "visible": true
}