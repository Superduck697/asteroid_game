{
    "id": "aa79ad52-184f-4ffc-93d4-8b3f84e7e5ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "0c2cbdc3-3e03-42ff-89bf-20fc9a007099",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aa79ad52-184f-4ffc-93d4-8b3f84e7e5ab"
        },
        {
            "id": "4dec2a8c-ba1a-480a-8573-3827e50cbe8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4ecd40f5-f347-4256-a465-1f1d63b40496",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aa79ad52-184f-4ffc-93d4-8b3f84e7e5ab"
        },
        {
            "id": "aec6da87-a23b-462d-bffb-838bae7c7a43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "aa79ad52-184f-4ffc-93d4-8b3f84e7e5ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a54a8861-b3a2-4e3f-afb4-8f4054c53a90",
    "visible": true
}